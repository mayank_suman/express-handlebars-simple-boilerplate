## A simple express handlebar boilerplate
---
**Steps to get started**

1. Run -- **git clone https://bitbucket.org/mayank_suman/express-handlebars-simple-boilerplate.git**
2. Then run -- **npm install**
3. Then start the server either by running -- **npm start** or **node server.js**
4. Now you could customize the boilerplate as you like.

